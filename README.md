# Blog Web Application -
Group 31

1) Title of project - 
Web applicaiton for user session management using JWT(json web token).

2) Group Name : Content is King
TY Comp DIV-2 Batch T2
Kishan Patel        : 111803172

3) Handling multiple requests to a web-based application or service from a 
single user or entity is an important task. Session management or Web based
Authentication is a basic component of security which has to be handled by 
web applicaitons to aviod various attacks such as insecure access, Cross-Site 
Request Forgery (CSRF), session hijacking, etc. Thus providing a secure Web
authentication to the active user availing the web services is the main of 
our project.

Current state of art to solve the problem -
4) HTTP being a stateless protocol which means it doesn't store its state from
the request or response. Thus the old school solution for providing web
authentication is using a session. A session is basically a piece of user data
stored on server side which remembers the user login. So anytime is a server
saves a user login, it sends a user a cookie which is ID proof which can be
referenced on the server againsts against the requests made by client. However
with different requirements like single application contacting mutiple backends
(since some servers are authentication based servers and some can be image
processing servers). In this conventional session-cookie based approach
doesn't work.

5) Implementation of new JWT(json web token) for web authentication. Instead of
setting a session for a user a server will send user a token, containing user
information, expire time information etc. Now user can use this token to access
the web services. Thus a single token can be used by multiple backends. JWT is
basically a security key(using the HMAC algorithm) used to encrypt JSON 
formatted data. Thus all the sensitive information is stored in the JWT in
hashed form and is send though the HTTP protocol to the user and user returns
the token back to the server for authentication and server will verify the
user. There is always a unique JWT for a unique user data.

6) Technology used - 
a) Frontend - HTML, CSS(framework - semantic UI), JS
b) Backend - Nodejs, MongoDB

7) Rating the content and reviews of users to give a user a particular rating
using NLP concept in Machine Learning. Thus the aim will be colaborating a web
application with Machine Learning.
